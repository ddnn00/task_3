﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace obj
{
    public class Item
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}