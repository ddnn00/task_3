﻿using System;
using ProgramCommand;

namespace ProgramControl
{
    public class Control
    {
        private readonly Command cmnd;

        public Control()
        {
            cmnd = new Command();
        }

        public void Menu()
        {
            var isMenuView = true;

            while (isMenuView)
            { 
                Console.WriteLine("* Введите номер команды:");
                Console.WriteLine("1 - create\t2 - read\t3 - update\t4 - delete");
                string command = Console.ReadLine();
                switch (command)
                {
                    case "1":
                        cmnd.Create();
                        break;
                    case "2":
                        cmnd.Read();
                        break;
                    case "3":
                        cmnd.Update();
                        break;
                    case "4":
                        cmnd.Delete();
                        break;
                    default:
                        Console.WriteLine("Неизвестная команда!");
                        break;
                }

                Console.WriteLine("* Хотите ввести еще команды?");
                Console.WriteLine("1 - да, 2 - нет");
                command = Console.ReadLine();
                if (command == "2")
                {
                    isMenuView = false;
                }
            }
        }
    }
}
