﻿using obj;
using System.Data.Entity;

namespace Context
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext() : base("DefaultConnection")
        {
        }
        public DbSet<Item> Items { get; set; }
        public object Item { get; internal set; }
    }
}