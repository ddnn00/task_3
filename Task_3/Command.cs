﻿using System;
using System.Linq;
using Context;
using obj;

namespace ProgramCommand
{
    public class Command
    {
        private readonly ApplicationContext db;
        public Command()
        {
            db = new ApplicationContext();
        }

        public void Create()
        {
            Console.WriteLine("* Введите название, которое хотите добавить в БД: ");
            var input = Console.ReadLine();

            var item = new Item()
            {
                Name = input
            };
            
            db.Items.Add(item);

            db.SaveChanges();
            
            Console.WriteLine("Данные успешно добавлены в БД!");
        }

        public void Read()
        {
            var items = db.Items
                .ToArray();

            foreach (Item u in items)
            {
                Console.WriteLine(u.Id + " " + u.Name);
            }
        }

        public void Update()
        {
            Console.WriteLine("* Введите Id в котором хотите изменить поле Name: ");
            int elem = int.Parse(Console.ReadLine());

            Console.WriteLine("Новое значение поля  Name: ");
            string propName = Console.ReadLine();

            var item = db.Items
                .Where(c => c.Id == elem)
                .FirstOrDefault();

            item.Name = propName;

            db.SaveChanges();
        }

        public void Delete()
        {
            Console.WriteLine("* Введите Id элемента, который хотите удалить: ");
            int elem = int.Parse(Console.ReadLine());
            
            Item item = db.Items
            .Where(o => o.Id == elem)
            .FirstOrDefault();

            Console.WriteLine($"Элемент с Id {elem} удален!");

            db.Items.Remove(item);
            db.SaveChanges();
        }
    }
}
