﻿using ProgramControl;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            var ctrl = new Control();

            ctrl.Menu();
        }
    }
}
